import { SundaySkyPage } from './app.po';

describe('sunday-sky App', function() {
  let page: SundaySkyPage;

  beforeEach(() => {
    page = new SundaySkyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
