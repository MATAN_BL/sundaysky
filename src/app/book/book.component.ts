import { Component, OnInit } from '@angular/core';
import {Service} from "../service.service";

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor(private service: Service) {
    console.log(this.service);
  }

  ngOnInit() {

  }

  changeSelection() {
    if (this.service.currentBook["selected"]) { // turn from 'checked' to 'unchecked'
      this.service.currentBook["selected"] = false;
      this.service.totalPrice -= parseInt(this.service.currentBook["price"]);
    } else { // turn from 'unchecked' to 'checked'
      this.service.currentBook["selected"] = true;
      this.service.totalPrice += parseInt(this.service.currentBook["price"]);
    }

  }

}
