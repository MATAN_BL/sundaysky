import { Component, OnInit } from '@angular/core';
import {Service} from "../service.service";

@Component({
  selector: 'selected-books',
  templateUrl: './selected-books.component.html',
  styleUrls: ['./selected-books.component.scss']
})
export class SelectedBooksComponent implements OnInit {

  constructor(private service: Service) { }

  ngOnInit() {
  }
  deleteBook(book: Object) {
    book['selected']=false;
    this.service.totalPrice -= parseInt(book['price']);
  }

}
