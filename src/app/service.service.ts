import { Injectable } from '@angular/core';
import { listOfBooks } from '../json/listOfBooks';
import { noListOfBooks } from '../json/noListOfBooks';
import { smallListOfBooks } from '../json/smallListOfBooks';

@Injectable()
export class Service {

  private _listOfBooks: Object;
  private _index: number;
  private _currentBook: Object;
  private _totalPrice: number;

  constructor() {
    this._listOfBooks = listOfBooks;
    this._index = 0;
    this.totalPrice = 0;
    this._currentBook = this._listOfBooks["books"][this._index];
    this.addSelectedField();
  }

  getPrevBook() {
    this._index = (this._index) + 1;
    this._index = ( (this._index == this._listOfBooks["books"].length) ? 0 : this._index);
    this._currentBook = this._listOfBooks["books"][this._index];
  }

  getNextBook() {
    this._index = this._index - 1;
    this._index = ( (this._index == -1) ? this._listOfBooks["books"].length - 1 : this._index);
    this._currentBook = this._listOfBooks["books"][this._index];
  }

  addSelectedField() {
    this._listOfBooks["books"].forEach((book: Object) => {
      book["selected"] = false;
    })
  }


  get listOfBooks(): Object {
    return this._listOfBooks;
  }

  set listOfBooks(value: Object) {
    this._listOfBooks = value;
  }

  get index(): number {
    return this._index;
  }

  set index(value: number) {
    this._index = value;
  }

  get currentBook(): Object {
    return this._currentBook;
  }

  set currentBook(value: Object) {
    this._currentBook = value;
  }

  get totalPrice(): number {
    return this._totalPrice;
  }

  set totalPrice(value: number) {
    this._totalPrice = value;
  }
}
